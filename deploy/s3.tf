resource "aws_s3_bucket" "app_public_files" {
  bucket        = "${local.prefix}-rw-20211201-files"
  acl           = "public-read"
  force_destroy = true
}
